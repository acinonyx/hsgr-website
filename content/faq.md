+++
title = "Frequently Asked Questions"
slug = "faq"
thumbnail = "images/tn.png"
description = "faq"
+++



#### How often is hackerspace open?

In theory the space is open 24/7. But if you want to be 100% sure you can check the events. If there is an event happening, then the space is definitely open. You can also check the counter on the top of the website, which is auto-updated with the current number of connected devices to the hackerspace network. And of course you can always call.


#### If I don't know any operator or community member can I come?

Of course! The space is open for everyone. If that's your first time here you can introduce yourself on arrival to get a short tour from an operator. And don't forget to read our vision.

#### What should I do to organize an event at the hackerspace?

Hackerspace is open to host any event which aligns with our vision. The important thing is to find one of the operators, who can open the space for the event. This can happen in person at the hackerspace, or through our discussion mailing list by sending an email explaining your event idea. Once you find an operator she/he can help you creating the event on the website and guide you through any logistics.

#### If I'm not a hacker can I come?

Hacking is about sharing and curiosity. Don't get intimidated by the term. If you are interested in sharing your knowledge or learn from others, then hackerspace is definitely the place to be. Our inspiration is the Open Source philosophy.
#### What does the space offers? What should I bring with me?

Besides getting to know and interact with interesting people and a vibrant community, the space offers free internet access, books and magazines you can read, workbenches and a great variety of tools and machinery. What you should bring is the will to communicate, share and optionally your laptop. You will also find the necessary infrastructure to get your caffeine fix and a fridge full of beers and beverages.
#### Who cleans up the space?

Everybody, including you. You are free to use anything available from the kitchen (coffee machines, glasses, dishes, etc). But remember, you should clean what you used and you should leave the space cleaner than you found it.
